const filterService = require('./filteringService');

const { from, timer  } = require('rxjs');
const { map, mergeAll } = require('rxjs/operators');

function getMyIP() {
    const mynetIp = require('os').networkInterfaces();
    return mynetIp['‏‏Ethernet'] ? mynetIp['‏‏Ethernet'][1].address : mynetIp['‏‏Wi-Fi'][1].address
}
// todo: get the destinaion form db
const PORTS = [3000, 8080, 8082, 80, 3123];
const Destinations = ['192.168.2.111', '192.168.2.109', '192.168.2.105'];

const DESPORT = from(Destinations.map(x => PORTS.map(y => ({ dest: x, port: y }))));

function executePortScanning(){
    DESPORT.pipe(mergeAll())
    .pipe(map( x=> {
        return  filterService(
            getMyIP(),
            x.dest,
            x.port,
            'some Payloads')
    }))
    .subscribe(
        async x => console.debug('++', await x),
        err => console.error('+++',err),
     () => {console.debug('compalte package now wating for more data to come') }
    )
}

// repate the scane every 10 sec
const source = timer(1000,10000);
const subscribe = source.subscribe(val => {
    console.log(`iteration ${val}`)
    return executePortScanning();
});
    