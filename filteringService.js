const fs = require('fs');
const log = require('./logger');
var net = require('net');

async function filterRequest(source, dest, port, payload){
  return await writeLegitimateRequest(source, dest, port, payload);
}

function writePortScanAttempt(source, dest, port, payload, message){
  log.error(`Port scan attempt from ${source} to ${dest}:${port} with payload ${JSON.stringify(payload)} got error ${message}`);
  log.error(`HISTORY ${new Error().stack}`);
}

async function writeLegitimateRequest(source, dest, port, payload){
  log.info(`Legitimate request from ${source} to ${dest}:${port}`);
  
  const _MAX_TIME_FOR_SOCKET = 1000;
  
  return await new Promise(async (resolve,reject)=> {
    try{
      var client = new net.Socket({writable: true,readable: true});
      client.setTimeout(_MAX_TIME_FOR_SOCKET);
  
      const  socket = await client.connect(port, dest)
  
      socket.on('connect', function(data) {
        console.log('Received: ' + data);
        socket.write(payload);
        client.destroy(); // kill client after server's response
        return resolve(data)
      });
  
      socket.on('error', error =>{
        // console.log(error.message)
        writePortScanAttempt(source, dest, port, payload,error.message);
        return resolve(error.message)
      })
      socket.on('timeout', () =>{
        // console.log(error.message)
        writePortScanAttempt(source, dest, port, payload,`custom connect TIMEOUT AFTER ${_MAX_TIME_FOR_SOCKET}`);
        return resolve(`TIMEOUT AFTER ${_MAX_TIME_FOR_SOCKET}`)
      })
      return socket;
    }catch(err){

      writePortScanAttempt(source, dest, port, payload,error.message);
      
      return resolve(err.message)
    }
  })
  
  
 
}
// Execute if ran directly for simple testing and debuging
if (typeof require !== 'undefined' && require.main === module) {

}

module.exports = filterRequest;